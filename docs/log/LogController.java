package log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/log")
public class LogController {

    @Autowired
    private LogRepository logRepository;

    @GetMapping(path = "/handshake")
    public String handShake(){
        return "handshake";
    }

    @PostMapping(path = "/weaver")
    public String logWeaver(WeaverLog log){
        return "OK";
    }


}
