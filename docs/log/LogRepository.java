package log;

import org.springframework.data.repository.CrudRepository;

public interface LogRepository extends CrudRepository<WeaverLog, Integer> {

}