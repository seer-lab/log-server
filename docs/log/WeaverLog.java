package log;


import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class WeaverLog {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @Column(name = "time", nullable = false)
    private LocalDateTime time;

    //Andrew, JR, Jan
    @Column(name="price", length= 10, nullable = false)
    private String owner;

    //local weaver, global weaver, log server
    @Column(name="server_type", length = 45, nullable = false)
    private String serverType;

    //gw-harvester, gw-data
    @Column(name = "module_id", length = 20, nullable = false)
    private String moduleId;

    @Column(name = "ip", length = 15, nullable = false)
    private String ip;

    @Column(name = "port", length = 5, nullable = false)
    private String port;

    //exception, log...
    @Column(name = "reason", length = 15, nullable = false)
    private String reason;

    @Column(name = "level", length = 10, nullable = false)
    private String level;

    @Column(name = "method", length = 10, nullable = false)
    private String method;

    @Column(name = "message", length = 10, nullable = false)
    private String message;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeaverLog)) return false;
        WeaverLog weaverLog = (WeaverLog) o;
        return Objects.equals(getId(), weaverLog.getId()) &&
                Objects.equals(getTime(), weaverLog.getTime()) &&
                Objects.equals(getOwner(), weaverLog.getOwner()) &&
                Objects.equals(getServerType(), weaverLog.getServerType()) &&
                Objects.equals(getModuleId(), weaverLog.getModuleId()) &&
                Objects.equals(getIp(), weaverLog.getIp()) &&
                Objects.equals(getPort(), weaverLog.getPort()) &&
                Objects.equals(getReason(), weaverLog.getReason()) &&
                Objects.equals(getLevel(), weaverLog.getLevel()) &&
                Objects.equals(getMethod(), weaverLog.getMethod()) &&
                Objects.equals(getMessage(), weaverLog.getMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTime(), getOwner(), getServerType(), getModuleId(), getIp(), getPort(), getReason(), getLevel(), getMethod(), getMessage());
    }
}
